@extends('layout.main')
@section('title')
	Tambah Cast
@endsection
@section('konten')
	<div class="card">
      
      <!-- /.card-header -->
      <div class="card-body">
        <form action="/cast" method="post">
          @csrf
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" required value="{{old('nama')}}">
          </div>
          <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" name="umur" id="umur" class="form-control" required value="{{old('umur')}}">
          </div>
          <div class="form-group">
            <label for="bio">Bio</label>
            <textarea name="bio" id="bio" class="form-control">{{old('bio')}}</textarea>
          </div>
          <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Simpan</button>
        </form>
      </div>
      <!-- /.card-body -->

</div>
@endsection