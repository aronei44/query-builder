@extends('layout.main')
@section('title')
	Data Cast
@endsection
@push('script')
<script src="/template/plugins/datatables/jquery.dataTables.js"></script>
<script src="/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css">

@endpush
@section('konten')
	<div class="card">
      <div class="card-header">
        <a href="/cast/create" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Data</a>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($casts as $cast)
              <tr>
                <td>{{$cast->nama}}</td>
                <td>{{$cast->umur}}</td>
                <td>{{$cast->bio}}</td>
                <td>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                  <form action="/cast/{{$cast->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->

</div>
@endsection