@extends('layout.main')
@section('title')
	<h1>Buat Account Baru!</h1>
	
@endsection
@section('konten')
	
	<h2>Sign Up Form</h2>
	<form action="/welcome" method="post">
		@csrf
		<div>
			<label for="first_name">First Name:</label><br>
			<input type="text" name="first_name" id="first_name" required>
		</div>
		<div>
			<label for="last_name">Last Name:</label><br>
			<input type="text" name="last_name" id="last_name" required>
		</div>
		<div>
			<p>Gender</p>
			<input type="radio" name="gender" value="male" id="male">
			<label for="male">Male</label><br>
			<input type="radio" name="gender" value="female" id="female">
			<label for="female">Female</label><br>
			<input type="radio" name="gender" value="other" id="other">
			<label for="other">Other</label>
		</div>
		<div>
			<label for="nationality">Nationality</label><br>
			<select name="nationality" id="nationality">
				<option value="Indonesian">Indonesian</option>
				<option value="English">English</option>
				<option value="Other">Other</option>
			</select>
		</div>
		<div>
			<p>Language Spoken:</p>
			<input type="checkbox" id="indonesia" name="bahasa" value="indonesia">
			<label for="indonesia">Bahasa Indonesia</label><br>
			<input type="checkbox" id="english" name="bahasa" value="english">
			<label for="english">English</label><br>
			<input type="checkbox" id="other1" name="bahasa" value="other">
			<label for="other1">Other</label><br>
		</div>
		<div>
			<label for="bio">Bio:</label><br>
			<textarea name="bio" id="bio"></textarea>
		</div>
		<button type="submit">Sign Up</button>
		<!-- <input type="submit"value="Sign Up"> -->
	</form>
@endsection