<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    	return view('layout.form');
    }
    public function welcome(Request $request){
    	return view('layout.welcomes',['request'=>$request]);
    }
}
