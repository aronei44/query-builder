<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
    	$casts = DB::table('casts')->get();
    	return view('layout.cast.index',['casts'=>$casts]);
    }
    public function create(){
    	return view('layout.cast.create');
    }
    public function store(Request $request){
    	$validatedData = $request->validate([
    		'nama' => 'required|max:45|unique:casts',
    		'umur' => 'required',
    		'bio'  => 'required'
    	]);
    	DB::table('casts')->insert([$validatedData]);
    	return redirect('/cast');
    }
    public function edit($cast_id){
    	$data = DB::table('casts')->find($cast_id);
    	return view('layout.cast.edit',['data'=>$data]);
    }
    public function update(Request $request, $cast_id){
    	$validatedData = $request->validate([
    		'nama' => 'required|max:45',
    		'umur' => 'required',
    		'bio'  => 'required'
    	]);
    	$data = DB::table('casts')->where('id',$cast_id)->update($validatedData);
    	return redirect('/cast');
    }
    public function delete($cast_id){
    	DB::table('casts')->where('id',$cast_id)->delete();
    	return back();
    }
}
